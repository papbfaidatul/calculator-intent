package com.example.calculatorintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {
    TextView Operasi, result;
    Button back;
    private String op,res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Bundle bun = getIntent().getExtras();
        op = bun.getString("op");
        res = bun.getString("res");
        Operasi = findViewById(R.id.operasi);
        Operasi.setText(op);
        result = findViewById(R.id.Result);
        result.setText(res);

        back = findViewById(R.id.buttonBack);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(MainActivity2.this,MainActivity.class);
        startActivity(intent);
    }
}
