package com.example.calculatorintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //inisiasi variabel
    TextView inputText;
    MaterialButton b0,b1,b2,b3,b4,b5,b6,b7,b8,b9;
    MaterialButton bDiv,bMultiple,bIs,bPlus,bMin;
    MaterialButton bOpen,bClose,bPlusMinus,bAC,bDecimal;

    String input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //connecting to view
        inputText = findViewById(R.id.numCol);

        assignId(b0,R.id.b0);
        assignId(b1,R.id.b1);
        assignId(b2,R.id.b2);
        assignId(b3,R.id.b3);
        assignId(b4,R.id.b4);
        assignId(b5,R.id.b5);
        assignId(b6,R.id.b6);
        assignId(b7,R.id.b7);
        assignId(b8,R.id.b8);
        assignId(b9,R.id.b9);

        assignId(bDiv,R.id.bDiv);
        assignId(bMultiple,R.id.bMultiple);
        assignId(bIs,R.id.bIs);
        assignId(bPlus,R.id.bPlus);
        assignId(bMin,R.id.bMin);

        assignId(bOpen,R.id.bOpen);
        assignId(bClose,R.id.bClose);
        assignId(bPlusMinus,R.id.bPlusMinus);
        assignId(bAC,R.id.bAC);
        assignId(bDecimal,R.id.bDecimal);
    }
    //definisikan method assignId
    void assignId(MaterialButton btn, int id){
        btn = findViewById(id);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        MaterialButton button = (MaterialButton) view;
        String in = button.getText().toString();
        String result = getResult(in);

        switch (in){
            case "AC":
                input="";
                break;
            case "1":
                input+="1";
                break;
            case "2":
                input+="2";
                break;
            case "3":
                input+="3";
                break;
            case "4":
                input+="4";
                break;
            case "5":
                input+="5";
                break;
            case "6":
                input+="6";
                break;
            case "7":
                input+="7";
                break;
            case "8":
                input+="8";
                break;
            case "9":
                input+="9";
                break;
            case "0":
                input+="0";
                break;

            case "*":
                input+="*";
                break;
            case "/":
                input+="/";
                break;
            case "+":
                input+="+";
                break;
            case "-":
                input+="-";
                break;
            case "(":
                input+="(";
                break;
            case ")":
                input+=")";
                break;
            case "+/-":
                input+="-";
                break;
            case ".":
                input+=".";
                break;

            case "=":
                Intent intent = new Intent(MainActivity.this,MainActivity2.class);
                String operasi = input.toString();
                String hasil = getResult(input).toString();
                intent.putExtra("op", operasi);
                intent.putExtra("res", hasil);
                startActivity(intent);
                break;

            default:
                if(input.startsWith("null")){
                    input = "";
                }
                break;
        }
        inputText.setText(input);
    }
    String getResult(String data){
        try {
            Context context = Context.enter();
            context.setOptimizationLevel(-1);

            Scriptable scriptable = context.initStandardObjects();
            String finalResult = context.evaluateString(scriptable,data,"Javascript", 1, null).toString();

            if(finalResult.endsWith(".0")){
                finalResult = finalResult.replace(".0","");
            }
            return finalResult;
        }
        catch (Exception e){
            return "Error";
        }

    }



}